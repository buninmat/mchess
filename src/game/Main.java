package game;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("viewMain.fxml"));
        stage.setTitle("mChess");
        stage.getIcons().add(new Image("icons/knight_w.png"));
        Scene scene  = new Scene(root);

        ObservableList<String> sheets = scene.getStylesheets();
        sheets.add(getClass().getResource("styles.css").toExternalForm());

        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
