package game;

import game.figures.*;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

public class FigureSelector {

    private GridPane pane;

    private boolean isWhite;

    private FigureDragBuffer figureDragBuffer;

    private int[] figuresCount = {1, 1, 2, 2, 2, 8};

    FigureSelector(GridPane pane, boolean isWhite, FigureDragBuffer figureDragBuffer) {
        this.pane = pane;
        this.isWhite = isWhite;
        this.figureDragBuffer = figureDragBuffer;
        addOnDrop();
    }

    private void addOnDrop(){
        pane.addEventHandler(DragEvent.DRAG_OVER, event -> {
            if(figureDragBuffer.getFigure().isWhite() == isWhite)
                event.acceptTransferModes(TransferMode.MOVE);
        });
        pane.addEventHandler(DragEvent.DRAG_DROPPED, event->addFigure(figureDragBuffer.fetch().getType()));
    }

    void show() {
        for (FigureType figure : FigureType.values()) {
            addFigureOnPane(figure);
        }
    }

    private void addFigureOnPane(FigureType figure) {
        int cnt = figuresCount[figure.ordinal()];
        if(cnt == 0)
            return;

        ImageView imgView = new ImageView(new Image(figure.getImageUrl(isWhite)));
        imgView.setFitWidth(45);
        imgView.setFitHeight(45);

        Label countLbl = new Label(String.format("x%d", cnt));

        StackPane stackPane = new StackPane();
        stackPane.getChildren().add(imgView);
        stackPane.getChildren().add(countLbl);

        EventHandler<MouseEvent> handler = event -> {
            Dragboard db = imgView.startDragAndDrop(TransferMode.MOVE);
            ClipboardContent content = new ClipboardContent();
            content.putImage(imgView.getImage());
            db.setContent(content);
            event.consume();

            figureDragBuffer.put(figure.makeInstance(isWhite), ()->removeFigure(figure));
        };

        stackPane.addEventHandler(MouseEvent.DRAG_DETECTED, handler);
        pane.add(stackPane, figure.ordinal(), 0);
    }

    private void addFigure(FigureType figure) {
        figuresCount[figure.ordinal()]++;
        show();
    }

    private void removeFigure(FigureType figure) {
        figuresCount[figure.ordinal()]--;
        pane.getChildren().clear();
        show();
    }
}
