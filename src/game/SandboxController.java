package game;

import game.board.Board;
import javafx.fxml.FXML;
import javafx.scene.layout.GridPane;

public class SandboxController {
    @FXML
    private GridPane boardPane;
    @FXML
    private GridPane figurePaneBlack;
    @FXML
    private GridPane figurePaneWhite;

    private BoardEditor boardEditor;

    private FigureSelector whiteFigureSelector;
    private FigureSelector blackFigureSelector;

    public SandboxController() {
    }

    @FXML
    void initialize() {
        FigureDragBuffer dragBuffer = new FigureDragBuffer();
        boardEditor = new BoardEditor(boardPane, dragBuffer);
        boardEditor.show(Board.getEmpty());
        whiteFigureSelector = initFigureSelector(true, dragBuffer);
        blackFigureSelector = initFigureSelector(false, dragBuffer);
        /*BoardController cntrl = new BoardController(boardPane);
        cntrl.show(Board.getClassic());*/
    }

    private FigureSelector initFigureSelector(boolean isWhite, FigureDragBuffer dragBuffer) {
        FigureSelector figureSelector = new FigureSelector(
                isWhite ? figurePaneWhite : figurePaneBlack,
                isWhite,
                dragBuffer);
        figureSelector.show();
        return figureSelector;
    }
}
