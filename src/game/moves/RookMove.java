package game.moves;

import game.board.Loc;

public class RookMove extends Move{
    public RookMove(Loc from, Loc to) {
        super(from, to);
    }

    @Override
    void make() {
        if(from.equals(new Loc(0,0))){
            board.setLongCastleWhite(false);
        }
        if(from.equals(new Loc(0,7))){
            board.setLongCastleBlack(false);
        }
        if(from.equals(new Loc(7,0))){
            board.setShortCastleWhite(false);
        }
        if(from.equals(new Loc(7,7))){
            board.setShortCastleBlack(false);
        }
        super.make();
    }
}
