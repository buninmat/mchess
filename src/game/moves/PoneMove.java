package game.moves;

import game.board.Loc;

public class PoneMove extends Move {
    public PoneMove(Loc from, Loc to) {
        super(from, to);
    }

    @Override
    void make() {
        replace();
    }
}
