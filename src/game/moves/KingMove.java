package game.moves;

import game.board.Loc;

public class KingMove extends Move {
    public KingMove(Loc from, Loc to) {
        super(from, to);
    }

    @Override
    void make() {
        disableCastles();
        super.make();
    }

    void disableCastles(){
        if (board.get(from).getFigure().isWhite()) {
            board.setLongCastleWhite(false);
            board.setShortCastleWhite(false);
        } else {
            board.setShortCastleBlack(false);
            board.setLongCastleBlack(false);
        }
    }
}
