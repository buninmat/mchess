package game.moves;

import game.board.Loc;

public class PoneDoubleMove extends PoneMove {

    public PoneDoubleMove(Loc from, Loc to) {
        super(from, to);
    }

    @Override
    void make() {
        replace();
        board.setPoneCrossTake(from.plus(new Loc(0, to.y > from.y ? 1 : -1)));
    }
}
