package game.moves;

import game.board.Loc;
import game.figures.Figure;
import game.figures.Queen;

public class PoneTransformationMove extends PoneMove {
    public PoneTransformationMove(Loc from, Loc to) {
        super(from, to);
    }

    @Override
    void make() {

        Figure newFigure;
        if(poneTransformationHandler != null)
            newFigure = poneTransformationHandler.getNewFigure();
        else
            newFigure = new Queen();

        board.get(to).setFigure(newFigure);
        board.get(from).removeFigure();

        if(replaceHandler != null)
            replaceHandler.run(from, to);
    }
}
