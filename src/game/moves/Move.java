package game.moves;

import game.board.Board;
import game.board.Field;
import game.board.Loc;
import game.figures.Figure;

public class Move {

    public final Loc from, to;

    Board board;

    public Move(Loc from, Loc to) {
        this.from = from;
        this.to = to;
    }

    public interface ReplaceHandler {
        void run(Loc from, Loc target);
    }
    ReplaceHandler replaceHandler;
    public void addReplaceHandler(ReplaceHandler handler){
        replaceHandler = handler;
    }

    public interface TakeHandler{
        void run(Figure figure, Loc loc);
    }
    private TakeHandler takeHandler;
    public void addTakeHandler(TakeHandler handler){
        takeHandler = handler;
    }

    public interface PoneTransformationHandler{
        Figure getNewFigure();
    }
    PoneTransformationHandler poneTransformationHandler;
    public void addPoneTransformationHandler(PoneTransformationHandler handler){
        poneTransformationHandler = handler;
    }

    public final void make(Board board){
        this.board = board;
        board.setPoneCrossTake(null);
        make();
    }

    void make(){
        take(to);
        replace();
    }

    void take(Loc loc){
        Field tarFld = board.get(loc);
        if(tarFld.hasFigure()) {
            if (takeHandler != null)
                takeHandler.run(tarFld.getFigure(), loc);
            tarFld.removeFigure();
        }
    }

    void replace() {
        Field srcFld = board.get(from);

        board.get(to).setFigure(srcFld.getFigure());
        srcFld.removeFigure();

        if(replaceHandler != null) {
            replaceHandler.run(from, to);
        }
    }
}
