package game.moves;

import game.board.Loc;

public class CrossTakeMove extends PoneMove {
    public CrossTakeMove(Loc from, Loc to) {
        super(from, to);
    }

    @Override
    void make() {
        replace();
        Loc takeLoc = new Loc(to.x, from.y);
        take(takeLoc);
        board.get(takeLoc).removeFigure();
    }
}
