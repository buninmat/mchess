package game.moves;

import game.board.Board;
import game.board.Loc;

public class CastleMove extends KingMove {

    public CastleMove(Loc from, Loc to) {
        super(from, to);
    }

    @Override
    void make() {
        disableCastles();
        replace();
        Loc dir = new Loc(to.x > from.x ? 1 : -1, 0);
        Loc jumpLoc = from.plus(dir);
        Loc rookLoc = to.plus(dir);
        //long castle
        if(dir.x < 0)
            rookLoc = rookLoc.plus(dir);
        board.get(jumpLoc).setFigure(board.get(rookLoc).getFigure());
        if (replaceHandler != null)
            replaceHandler.run(rookLoc, jumpLoc);
        board.get(rookLoc).removeFigure();
    }

    public boolean isBlocked(Board board){
        boolean isLong = from.x > to.x;
        boolean isWhite = from.y == 0;
        if(isLong) {
            if(isWhite)
                return !board.isLongCastleWhite();
            return !board.isLongCastleBlack();
        }
        if(isWhite)
            return !board.isShortCastleWhite();
        return !board.isShortCastleBlack();
    }
}
