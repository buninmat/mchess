package game;

import game.board.Board;
import game.board.Field;
import game.board.Loc;
import game.figures.Figure;
import game.figures.FigureType;
import game.figures.King;
import javafx.css.PseudoClass;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.input.*;
import javafx.scene.layout.GridPane;

public class BoardEditor {
    private Board board;

    private GridPane boardPane;

    private BoardDrawer boardDrawer;

    private FigureDragBuffer figureDragBuffer;

    private PseudoClass placeFieldsPseudo = PseudoClass.getPseudoClass("show-mnemonic");

    BoardEditor(GridPane pane, FigureDragBuffer figureDragBuffer) {
        this.figureDragBuffer = figureDragBuffer;
        boardDrawer = new BoardDrawer(pane,
                this::setOnDrop,
                this::setOnDrag);
    }

    private void setOnDrag(Node figureNode, Loc loc) {
        figureNode.addEventHandler(MouseEvent.DRAG_DETECTED, event -> {

            Dragboard db = figureNode.startDragAndDrop(TransferMode.MOVE);
            ClipboardContent content = new ClipboardContent();

            Figure dragged = board.get(loc).getFigure();
            figureDragBuffer.put(dragged, () -> {
                boardDrawer.removeFigure(loc);
                board.get(loc).removeFigure();
            });

            content.putImage(new Image(dragged.getImageUrl()));
            db.setContent(content);
            event.consume();
        });
    }

    private void setOnDrop(Node field, Loc loc) {

        field.addEventHandler(DragEvent.DRAG_DROPPED, event -> {
            board.get(loc).setFigure(figureDragBuffer.fetch());
            boardDrawer.addFigure(loc);
        });

        field.addEventHandler(DragEvent.DRAG_ENTERED,
                event -> {
                    if (isValidPlacing(loc))
                        field.pseudoClassStateChanged(placeFieldsPseudo, true);
                });

        field.addEventHandler(DragEvent.DRAG_EXITED,
                event -> field.pseudoClassStateChanged(placeFieldsPseudo, false));

        field.addEventHandler(DragEvent.DRAG_OVER, event -> {
            if (isValidPlacing(loc))
                event.acceptTransferModes(TransferMode.ANY);

            event.consume();
        });
    }

    private boolean isValidPlacing(Loc loc) {
        return checkValidKingPlacing(loc, figureDragBuffer.getFigure())
                && checkValidPonePlacing(loc, figureDragBuffer.getFigure());
    }

    private boolean checkValidKingPlacing(Loc loc, Figure figure) {
        if (figure.getType() == FigureType.KING) {
            for (Loc dir : King.royalDirs) {
                Field nhb = board.get(loc.plus(dir));
                if (nhb != null && nhb.hasFigure()
                        && figure.isWhite() != nhb.getFigure().isWhite()
                        && nhb.getFigure().getType() == FigureType.KING)
                    return false;
            }
        }
        return true;
    }

    private boolean checkValidPonePlacing(Loc loc, Figure figure) {
        if (figure.getType() != FigureType.PONE)
            return true;

        return figure.isWhite() ? (loc.y != 0) : (loc.y != 7);
    }

    void show(Board board) {
        this.board = board;
        boardDrawer.draw(board, 500);
    }

    public Board getBoard() {
        return board;
    }
}
