package game.figures;

import game.board.Loc;
import game.moves.Move;

import java.util.ArrayList;

public final class Bishop extends Figure {

    @Override
    public FigureType getType() {
        return FigureType.BISHOP;
    }

    @Override
    protected ArrayList<Move> getAllMoves(Loc from) {
        return getMoveLines(Move::new, from, Loc.bDiag, Loc.wDiag, Loc.bDiag.getInv(), Loc.wDiag.getInv());
    }
}
