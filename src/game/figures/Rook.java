package game.figures;

import game.board.Loc;
import game.moves.Move;
import game.moves.RookMove;

import java.util.ArrayList;

public final class Rook extends Figure {

    @Override
    public FigureType getType() {
        return FigureType.ROOK;
    }

    @Override
    protected ArrayList<Move> getAllMoves(Loc from) {
        return getMoveLines(RookMove::new, from, Loc.horiz, Loc.vert, Loc.horiz.getInv(), Loc.vert.getInv());
    }
}
