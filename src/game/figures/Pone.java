package game.figures;

import game.board.Field;
import game.board.Loc;
import game.moves.*;

import java.util.ArrayList;

public final class Pone extends Figure {

    @Override
    public FigureType getType() {
        return FigureType.PONE;
    }

    @Override
    protected ArrayList<Move> getAllMoves(Loc from) {
        ArrayList<Move> moves = new ArrayList<>(4);
        for (Loc dir : new Loc[]{Loc.wDiag, Loc.bDiag, Loc.vert}) {
            Loc to = from.plus(isWhite() ? dir : dir.getInv());
            if (!to.isValid())
                continue;
            Field target = board.get(to);

            if (dir.equals(Loc.vert)) {
                if (!target.hasFigure()) {

                    if (isWhite() ? (to.y == 7) : (to.y == 0))
                        moves.add(new PoneTransformationMove(from, to));
                    else
                        moves.add(new PoneMove(from, to));

                    Loc doubleMove = from.plus(isWhite() ? new Loc(0, 2) : new Loc(0, -2));
                    if (doubleMove.isValid()
                            && (isWhite() ? (from.y == 1) : (from.y == 6))
                            && !board.get(doubleMove).hasFigure())
                        moves.add(new PoneDoubleMove(from, doubleMove));
                }

            } else if (isOpponent(target.getFigure())) {
                moves.add(new Move(from, to));
            } else {
                attack(to);

                if (board.getPoneCrossTake() != null
                        && board.getPoneCrossTake().equals(to)
                        && board.getPoneCrossTake().y == (isWhite() ? 5 : 2)) {
                    moves.add(new CrossTakeMove(from, to));
                }
            }
        }

        return moves;
    }
}