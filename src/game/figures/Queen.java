package game.figures;

import game.board.Loc;
import game.moves.Move;

import java.util.ArrayList;

public final class Queen extends Figure {

    @Override
    public FigureType getType() {
        return FigureType.QUEEN;
    }

    @Override
    protected ArrayList<Move> getAllMoves(Loc from) {
        return getMoveLines(Move::new, from, King.royalDirs);
    }
}
