package game.figures;

import game.board.Board;
import game.board.Field;
import game.board.Loc;
import game.moves.Move;
import java.util.ArrayList;

public abstract class Figure {

    private boolean isWhite;

    Board board;

    public Figure white() {
        this.isWhite = true;
        return this;
    }

    public Figure black() {
        this.isWhite = false;
        return this;
    }

    public boolean isWhite() {
        return isWhite;
    }

    boolean isOpponent(Figure figure){
        return figure != null && figure.isWhite != isWhite;
    }

    public String getImageUrl(){
        return getType().getImageUrl(isWhite);
    }

    public abstract FigureType getType();

    private boolean addAttacks;

    public ArrayList<Move> getMovesOnBoard(Loc from, Board board) {
        this.board = board;
        addAttacks = false;
        ArrayList<Move> moves = getAllMoves(from);
        for(int i = 0; i < moves.size(); i++) {
            Field target = board.get(moves.get(i).to);
            if (target.hasFigure() && target.getFigure() instanceof King) {
                moves.remove(i);
                break;
            }
        }
        return moves;
    }

    public void addAttacksOnBoard(Loc from, Board board){
        this.board = board;
        addAttacks = true;
        getAllMoves(from);
    }

    protected abstract ArrayList<Move> getAllMoves(Loc from);

    void attack(Loc loc){
        Field target = board.get(loc);
        if(isOpponent(target.getFigure()) && target.getFigure() instanceof King)
            board.setCheckKingLoc(loc);
        if(addAttacks)
            board.addAttack(loc);
    }

    interface MoveCreator {
        Move getMove(Loc from, Loc to);
    }

    ArrayList<Move> getUnitMoves(MoveCreator moveCreator, Loc from, Loc... dirs) {
        ArrayList<Move> valid = new ArrayList<>(dirs.length);
        for (Loc dir : dirs) {
            Loc to = from.plus(dir);
            Field target = board.get(to);
            if (target != null && (!target.hasFigure() || isOpponent(target.getFigure()))) {
                valid.add(moveCreator.getMove(from, to));
                attack(to);
            }
        }
        return valid;
    }

    ArrayList<Move> getMoveLines(MoveCreator moveCreator, Loc from, Loc... dirs) {
        ArrayList<Move> moves = new ArrayList<>(31);
        for (Loc dir : dirs) {
            Loc to = from;
            while (true) {
                to = to.plus(dir);

                Field target = board.get(to);
                if (target == null)
                    break;

                if (target.hasFigure()) {
                    if (isOpponent(target.getFigure())) {
                        moves.add(moveCreator.getMove(from, to));
                        attack(to);
                    }
                    break;
                }
                moves.add(moveCreator.getMove(from, to));
                attack(to);
            }
        }
        return moves;
    }
}
