package game.figures;

import game.board.Loc;
import game.moves.Move;

import java.util.ArrayList;

public final class Knight extends Figure {

    @Override
    public FigureType getType() {
        return FigureType.KNIGHT;
    }

    @Override
    protected ArrayList<Move> getAllMoves(Loc from) {
        Loc[] dirs = {
                new Loc(2, -1),
                new Loc(-2, -1),
                new Loc(2, 1),
                new Loc(-2, 1),
                new Loc(1, -2),
                new Loc(-1, -2),
                new Loc(1, 2),
                new Loc(-1, 2)};

        return getUnitMoves(Move::new, from, dirs);
    }
}
