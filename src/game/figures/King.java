package game.figures;

import game.board.Loc;
import game.moves.CastleMove;
import game.moves.KingMove;
import game.moves.Move;

import java.util.ArrayList;

public final class King extends Figure {

    public static Loc[] royalDirs = {Loc.bDiag, Loc.wDiag, Loc.bDiag.getInv(), Loc.wDiag.getInv(),
            Loc.horiz, Loc.vert, Loc.horiz.getInv(), Loc.vert.getInv()};

    @Override
    public FigureType getType() {
        return FigureType.KING;
    }

    @Override
    protected ArrayList<Move> getAllMoves(Loc from) {

        ArrayList<Move> moves = getUnitMoves(KingMove::new, from, royalDirs);

        Loc shortCastle = from.plus(new Loc(2, 0));
        if (isValidCastle(shortCastle, new Loc(1, 0).plus(from), new Loc(3, 0).plus(from))) {
            moves.add(new CastleMove(from, shortCastle));
        }

        Loc longCastle = from.plus(new Loc(-2, 0));
        if (isValidCastle(longCastle, new Loc(-1, 0).plus(from), new Loc(-4, 0).plus(from))) {
            moves.add(new CastleMove(from, longCastle));
        }

        return moves;
    }

    private boolean isValidCastle(Loc to, Loc jump, Loc rookLoc) {
        return rookLoc.isValid() && to.isValid() && jump.isValid()
                && !board.get(to).hasFigure() && !board.get(jump).hasFigure();
    }
}
