package game.figures;

public enum FigureType {
    KING(King::new, "icons/king_w.png", "icons/king_b.png"),
    QUEEN(Queen::new, "icons/queen_w.png", "icons/queen_b.png"),
    BISHOP(Bishop::new, "icons/bishop_w.png", "icons/bishop_b.png"),
    KNIGHT(Knight::new, "icons/knight_w.png", "icons/knight_b.png"),
    ROOK(Rook::new, "icons/rook_w.png", "icons/rook_b.png"),
    PONE(Pone::new, "icons/pone_w.png", "icons/pone_b.png");

    private FigureFactory figureFactory;
    public final String whiteImageUrl;
    public final String blackImageUrl;

    private interface FigureFactory{
        Figure make();
    }

    FigureType(FigureFactory figureFactory, String whiteImageUrl, String blackImageUrl) {
        this.figureFactory = figureFactory;
        this.whiteImageUrl = whiteImageUrl;
        this.blackImageUrl = blackImageUrl;
    }

    public Figure makeInstance(boolean isWhite) {
        return isWhite ? figureFactory.make().white() : figureFactory.make().black();
    }

    public String getImageUrl(boolean isWhite){
        return isWhite ? whiteImageUrl : blackImageUrl;
    }
}