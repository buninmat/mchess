package game;

import game.board.Board;
import game.board.Field;
import game.board.Loc;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.TextAlignment;

class BoardDrawer {

    private GridPane boardPane;

    private double notationS, fieldS, figureS;

    private Board board;

    private AddHandler addFieldHandler, addFigureHandler;

    BoardDrawer(GridPane boardPane, AddHandler addFieldHandler, AddHandler addFigureHandler) {
        this.boardPane = boardPane;
        this.addFieldHandler = addFieldHandler;
        this.addFigureHandler = addFigureHandler;
    }

    interface AddHandler{
        void run(Node node, Loc loc);
    }

    void draw(Board board, double size) {

        this.board = board;
        calculateParams(size);

        boardPane.setPrefHeight(size);
        boardPane.setPrefWidth(size);
        for (int i = 0; i <= 8; i++)
            for (int j = 0; j <= 8; j++) {

                if (i == 8 ^ j == 0) {

                    Label lbl = new Label();
                    lbl.setTextAlignment(TextAlignment.JUSTIFY);
                    lbl.setAlignment(Pos.BASELINE_CENTER);
                    lbl.getStyleClass().add("notation_label");

                    if (j == 0) {
                        lbl.setText(String.valueOf(8 - i));
                        lbl.setPrefWidth(notationS);
                        lbl.setPrefHeight(fieldS);


                    } else {
                        lbl.setText(String.valueOf((char) ('A' + j - 1)));
                        lbl.setPrefHeight(notationS);
                        lbl.setPrefWidth(fieldS);
                    }

                    boardPane.add(lbl, j, i);
                    continue;
                }

                if (i == 8)
                    continue;

                Loc loc = new Loc(j, i);
                addField(loc);
                addFigureToGrid(loc);
            }
    }

    private void calculateParams(double size) {
        notationS = 25;
        size -= notationS;
        fieldS = size / 8;
        figureS = fieldS - 3;
    }

    Button getField(Loc boardLoc) {
        return (Button) boardPane.lookup(String.format("#%s", boardLoc.toString()));
    }

    Button getFigure(Loc boardLoc) {
        return (Button) boardPane.lookup(String.format("#f%s", boardLoc.toString()));
    }

    private ImageView getFigureImage(Loc boardLoc) {
        return (ImageView) boardPane.lookup(String.format("#fi%s", boardLoc.toString()));
    }

    private static Loc toGridLoc(Loc loc) {
        return new Loc(loc.x + 1, 7 - loc.y);
    }

    private static Loc toBoardLoc(Loc loc) {
        return new Loc(loc.x - 1, 7 - loc.y);
    }

    private void addField(Loc gridLoc) {
        Button field = new Button();
        field.setId(String.format("%s", toBoardLoc(gridLoc).toString()));
        field.setPrefHeight(fieldS);
        field.setPrefWidth(fieldS);
        field.getStyleClass().add((gridLoc.y + gridLoc.x) % 2 != 0 ? "white_field" : "black_field");
        boardPane.add(field, gridLoc.x, gridLoc.y);

        if(addFieldHandler != null)
            addFieldHandler.run(field, toBoardLoc(gridLoc));
    }

    void addFigure(Loc loc){
        addFigureToGrid(toGridLoc(loc));
    }

    private void addFigureToGrid(Loc gridLoc) {
        Field fld = board.get(toBoardLoc(gridLoc));
        if (fld.hasFigure()) {

            ImageView figImg = new ImageView(new Image(fld.getFigure().getImageUrl()));
            figImg.setId(String.format("fi%s", toBoardLoc(gridLoc).toString()));
            figImg.setFitHeight(figureS);
            figImg.setFitWidth(figureS);
            GridPane.setValignment(figImg, VPos.CENTER);
            GridPane.setHalignment(figImg, HPos.CENTER);
            boardPane.add(figImg, gridLoc.x, gridLoc.y);

            Button bt = new Button();
            bt.setId(String.format("f%s", toBoardLoc(gridLoc).toString()));
            bt.setPrefWidth(fieldS);
            bt.setPrefHeight(fieldS);
            bt.getStyleClass().add("figure-button");
            boardPane.add(bt, gridLoc.x, gridLoc.y);

            if(addFigureHandler != null)
                addFigureHandler.run(bt, toBoardLoc(gridLoc));
        }
    }

    void removeFigure(Loc loc) {
        Button fig = getFigure(loc);
        boardPane.getChildren().remove(fig);
        boardPane.getChildren().remove(getFigureImage(loc));
    }

}
