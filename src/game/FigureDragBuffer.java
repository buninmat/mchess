package game;

import game.figures.Figure;

class FigureDragBuffer{
    private Figure figure;
    private DropAction onDropped;

    Figure fetch() {
        if(onDropped != null)
            onDropped.run();
        Figure res = figure;
        figure = null;
        onDropped = null;
        return res;
    }

    interface DropAction{
        void run();
    }

    void put(Figure figure, DropAction onDropped){
        this.figure = figure;
        this.onDropped = onDropped;
    }

    Figure getFigure(){
        return figure;
    }
}