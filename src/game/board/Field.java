package game.board;

import game.figures.Figure;

public class Field {
    private Figure figure;

    Field(){}

    Field(Field other) {
        this.figure = other.figure;
    }

    public void setFigure(Figure figure) {
        this.figure = figure;
    }

    public Figure getFigure() {
        return figure;
    }

    public void removeFigure() {
        figure = null;
    }

    public boolean hasFigure() {
        return figure != null;
    }
}
