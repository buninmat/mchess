package game.board;

public class Loc {
    public final int x;
    public final int y;

    public Loc(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Loc vert = new Loc(0, 1);

    public static Loc horiz = new Loc(1, 0);

    public static Loc bDiag = new Loc(1, 1);

    public static Loc wDiag = new Loc(-1, 1);

    public Loc plus(Loc loc) {
        return new Loc(loc.x + x, loc.y + y);
    }

    public Loc getInv() {
        return new Loc(-x, -y);
    }

    public boolean isValid() {
        return x >= 0 && x < 8 && y >= 0 && y < 8;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != getClass())
            return false;
        Loc other = (Loc) obj;
        return other.x == x && other.y == y;
    }

    @Override
    public int hashCode() {
        return x << 4 + y;
    }

    @Override
    public String toString() {
        return String.format("%c%d", (char)(x + 'a'), y + 1);
    }
}
