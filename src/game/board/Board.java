package game.board;

import game.figures.*;

import java.util.HashSet;

public class Board {
    private Field[][] data;

    private boolean bLongCastle, bShortCastle, wLongCastle, wShortCastle;

    private Loc checkKingLoc;

    private Loc poneCrossTake;

    private HashSet<Loc> attacks = new HashSet<>(64);

    private Board() {
        data = new Field[8][8];
        all((fld, loc) -> data[loc.x][loc.y] = new Field());
    }

    public Board(Board other) {
        data = new Field[8][8];
        all((fld, loc) -> data[loc.x][loc.y] = new Field(other.data[loc.x][loc.y]));
        this.bLongCastle = other.bLongCastle;
        this.bShortCastle = other.bShortCastle;
        this.wLongCastle = other.wLongCastle;
        this.wShortCastle = other.wShortCastle;
    }

    public Field get(Loc loc) {
        if (!loc.isValid())
            return null;
        else
            return data[loc.y][loc.x];
    }

    public Field get(int x, int y) {
        return get(new Loc(x, y));
    }

    public void setCheckKingLoc(Loc checkKingLoc) {
        this.checkKingLoc = checkKingLoc;
    }

    public Loc getCheckKingLoc() {
        return checkKingLoc;
    }

    public void addAttack(Loc loc) {
        attacks.add(loc);
    }

    public void clearAttacks() {
        attacks.clear();
    }

    public void setPoneCrossTake(Loc loc) {
        poneCrossTake = loc;
    }

    public Loc getPoneCrossTake() {
        return poneCrossTake;
    }

    public boolean isLongCastleBlack() {
        return bLongCastle && !attacks.contains(new Loc(3, 7));
    }

    public boolean isShortCastleBlack() {
        return bShortCastle && !attacks.contains(new Loc(5, 7));
    }

    public boolean isLongCastleWhite() {
        return wLongCastle && !attacks.contains(new Loc(3, 0));
    }

    public boolean isShortCastleWhite() {
        return wShortCastle && !attacks.contains(new Loc(5, 0));
    }

    public void setLongCastleBlack(boolean bLongCastle) {
        this.bLongCastle = bLongCastle;
    }

    public void setShortCastleBlack(boolean bShortCastle) {
        this.bShortCastle = bShortCastle;
    }

    public void setLongCastleWhite(boolean wLongCastle) {
        this.wLongCastle = wLongCastle;
    }

    public void setShortCastleWhite(boolean wShortCastle) {
        this.wShortCastle = wShortCastle;
    }

    public static Board getClassic() {

        Figure[] setA = new Figure[]{
                new Rook(), new Knight(), new Bishop(), new Queen(),
                new King(), new Bishop(), new Knight(), new Rook()};

        Figure[] setB = new Figure[]{
                new Rook(), new Knight(), new Bishop(), new Queen(),
                new King(), new Bishop(), new Knight(), new Rook()};

        Board classic = new Board();
        for (int i = 0; i <= 7; i++) {
            classic.get(i, 0).setFigure(setA[i].white());
            classic.get(i, 7).setFigure(setB[i].black());
            classic.get(i, 1).setFigure(new Pone().white());
            classic.get(i, 6).setFigure(new Pone().black());
        }

        classic.bShortCastle = classic.bLongCastle = classic.wLongCastle = classic.wShortCastle = true;

        return classic;
    }

    public static Board getEmpty(){
        return new Board();
    }

    public interface Action {
        void run(Field fld, Loc loc);
    }

    public void all(Action action) {
        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++) {
                Loc loc = new Loc(j, i);
                action.run(get(loc), loc);
            }
    }
}
