package game;

import game.board.Board;
import game.board.Loc;
import game.figures.*;
import javafx.css.PseudoClass;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

import java.util.HashMap;

class BoardController {

    private GridPane boardPane;

    private Game game;

    private BoardDrawer boardDrawer;

    private PseudoClass moveFieldsPseudo = PseudoClass.getPseudoClass("show-mnemonic");
    private PseudoClass targetFigurePseudo = PseudoClass.getPseudoClass("disabled");
    private PseudoClass[] checkPseudo = {PseudoClass.getPseudoClass("disabled"), PseudoClass.getPseudoClass("show-mnemonic")};

    private EventHandler<MouseEvent> cancelHandler;

    private HashMap<Loc, EventHandler<MouseEvent>> makeMoveHandlers;

    private HashMap<Loc, EventHandler<MouseEvent>> showMovesHandlers;

    BoardController(GridPane boardPane) {
        this.boardPane = boardPane;
        makeMoveHandlers = new HashMap<>();
        showMovesHandlers = new HashMap<>();
    }

    void show(Board board){
        initBoardDrawer(board);
        initGame(board);
    }

    private void initBoardDrawer(Board board) {
        cancelHandler = event -> {
            setMovesShowed(null);
            cleanupHandlers();
        };

        BoardDrawer.AddHandler addFieldHandler = (node, loc) -> node.addEventHandler(MouseEvent.MOUSE_PRESSED, cancelHandler);

        BoardDrawer.AddHandler addFigureHandler = (node, loc)-> {
            EventHandler<MouseEvent> showMovesHandler = event->showMoves(loc);
            showMovesHandlers.put(loc, showMovesHandler);
            node.addEventHandler(MouseEvent.MOUSE_PRESSED, showMovesHandler);
        };

        boardDrawer = new BoardDrawer(boardPane, addFieldHandler, addFigureHandler);
        boardDrawer.draw(board, 500);
    }

    private void initGame(Board board){
        this.game = new Game(board);
        setGameHandlers();
        game.update();
    }

    private void setGameHandlers() {
        game.setMoveReplaceHandler((from, to) -> {

            setCheck(null);
            setMovesShowed(null);
            cleanupHandlers();

            boardDrawer.addFigure(to);
            boardDrawer.removeFigure(from);
            showMovesHandlers.remove(from);
        });

        game.setMoveTakeHandler((fig, loc) -> {

            cleanupHandlers();

            Button takenFig = boardDrawer.getFigure(loc);
            if (takenFig != null) {
                boardDrawer.removeFigure(loc);
                showMovesHandlers.remove(loc);
            }
        });

        game.setPoneTransformationHandler(Queen::new);

        game.setCheckHandler(this::setCheck);
        game.setMateHandler((kingLoc) -> {
            setCheck(kingLoc);
        });
    }

    private Loc kingLoc;

    private void setCheck(Loc loc) {
        if (loc != null) {
            boardDrawer.getField(loc).pseudoClassStateChanged(checkPseudo[1], true);
            boardDrawer.getField(loc).pseudoClassStateChanged(checkPseudo[0], true);
        } else if (kingLoc != null) {
            boardDrawer.getField(kingLoc).pseudoClassStateChanged(checkPseudo[1], false);
            boardDrawer.getField(kingLoc).pseudoClassStateChanged(checkPseudo[0], false);
        }
        kingLoc = loc;
    }

    private Loc movesShowedLoc;

    private void setMovesShowed(Loc loc) {
        if (loc != null) {
            boardDrawer.getField(loc).pseudoClassStateChanged(moveFieldsPseudo, true);
        } else if (movesShowedLoc != null) {
            boardDrawer.getField(movesShowedLoc).pseudoClassStateChanged(moveFieldsPseudo, false);
        }

        movesShowedLoc = loc;
    }

    private void showMoves(Loc from) {

        if (game.isWhiteTurn() != game.getMatrix().get(from).getFigure().isWhite())
            return;

        cleanupHandlers();

        if (movesShowedLoc != null) {
            setMovesShowed(null);
            return;
        }

        for (Loc to : game.getMoves(from)) {

            EventHandler<MouseEvent> moveHandler = event -> game.makeMove(from, to);
            makeMoveHandlers.put(to, moveHandler);

            Button fld = boardDrawer.getField(to);
            Button fig = boardDrawer.getFigure(to);
            if (fig != null) {
                fig.addEventHandler(MouseEvent.MOUSE_CLICKED, moveHandler);
                fig.removeEventHandler(MouseEvent.MOUSE_PRESSED, showMovesHandlers.get(to));
                fld.pseudoClassStateChanged(targetFigurePseudo, true);
            } else {
                fld.removeEventHandler(MouseEvent.MOUSE_PRESSED, cancelHandler);
                fld.addEventHandler(MouseEvent.MOUSE_CLICKED, moveHandler);
                fld.pseudoClassStateChanged(moveFieldsPseudo, true);
            }
        }

        setMovesShowed(from);
    }

    private void cleanupHandlers() {
        for (HashMap.Entry<Loc, EventHandler<MouseEvent>> handler : makeMoveHandlers.entrySet()) {

            Button fld = boardDrawer.getField(handler.getKey());
            Button fig = boardDrawer.getFigure(handler.getKey());
            if (fig != null) {
                fig.removeEventHandler(MouseEvent.MOUSE_CLICKED, handler.getValue());
                fig.addEventHandler(MouseEvent.MOUSE_PRESSED, showMovesHandlers.get(handler.getKey()));
                fld.pseudoClassStateChanged(targetFigurePseudo, false);
            } else {
                fld.removeEventHandler(MouseEvent.MOUSE_CLICKED, handler.getValue());
                fld.addEventHandler(MouseEvent.MOUSE_PRESSED, cancelHandler);
                fld.pseudoClassStateChanged(moveFieldsPseudo, false);
            }
        }

        makeMoveHandlers.clear();
    }
}
