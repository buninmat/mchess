package game;

import game.board.Board;
import game.board.Loc;
import game.moves.CastleMove;
import game.moves.Move;

import java.util.ArrayList;
import java.util.HashMap;

public class Game {

    private Board matrix;

    private boolean isWhiteTurn = true;

    public interface CheckMateHandler {
        void run(Loc kingLoc);
    }

    private CheckMateHandler mateHandler;
    private CheckMateHandler checkHandler;
    private Move.ReplaceHandler moveReplaceHandler;
    private Move.TakeHandler moveTakeHandler;
    private Move.PoneTransformationHandler poneTransformationHandler;

    private HashMap<Loc, ArrayList<Move>> possibleMoves;

    public Game(Board matrix) {
        this.matrix = matrix;
        possibleMoves = new HashMap<>();
    }

    public void setMateHandler(CheckMateHandler handler) {
        mateHandler = handler;
    }

    public void setCheckHandler(CheckMateHandler handler) {
        checkHandler = handler;
    }

    public void setMoveTakeHandler(Move.TakeHandler handler){
        moveTakeHandler = handler;
    }

    public void setMoveReplaceHandler(Move.ReplaceHandler handler){
        moveReplaceHandler = handler;
    }

    public void setPoneTransformationHandler(Move.PoneTransformationHandler handler){
        poneTransformationHandler = handler;
    }

    public Board getMatrix() {
        return matrix;
    }

    public boolean isWhiteTurn() {
        return isWhiteTurn;
    }

    public ArrayList<Loc> getMoves(Loc loc) {
        ArrayList<Move> moves = possibleMoves.get(loc);
        if (moves == null)
            return new ArrayList<>();
        ArrayList<Loc> locations = new ArrayList<>(moves.size());
        for (Move move : moves)
            locations.add(move.to);
        return locations;
    }

    public void update(){
        updateAttacks(matrix, isWhiteTurn);
        updatePossMoves();
    }

    private void updateAttacks(Board matrix, boolean isWhiteTurn) {
        matrix.clearAttacks();
        matrix.all((fld, loc) -> {
            if (fld.hasFigure() && fld.getFigure().isWhite() == !isWhiteTurn) {
                fld.getFigure().addAttacksOnBoard(loc, matrix);
            }
        });
    }

    private void updatePossMoves(){
        possibleMoves.clear();
        matrix.all((fld, loc) -> {
            if (fld.hasFigure() && fld.getFigure().isWhite() == isWhiteTurn) {
                ArrayList<Move> moves = new ArrayList<>(64);
                for(Move move : fld.getFigure().getMovesOnBoard(loc, matrix)) {
                    if(selfCheck(move)
                            || (move instanceof CastleMove && ((CastleMove) move).isBlocked(matrix)))
                        continue;
                    move.addReplaceHandler(moveReplaceHandler);
                    move.addTakeHandler(moveTakeHandler);
                    move.addPoneTransformationHandler(poneTransformationHandler);
                    moves.add(move);
                }
                if(moves.size() != 0)
                    possibleMoves.put(loc, moves);
            }
        });
    }

    private boolean selfCheck(Move move){
            Board copy = new Board(matrix);
            move.make(copy);
            updateAttacks(copy, isWhiteTurn);
            return copy.getCheckKingLoc() != null;
    }

    public void makeMove(Loc from, Loc to) {
        if (from.equals(to))
            return;

        ArrayList<Move> moves = possibleMoves.get(from);
        if (moves == null)
            return;

        boolean moveFound = false;
        for (Move move : moves)
            if (move.to.equals(to)) {

                move.make(matrix);

                moveFound = true;
                break;
            }

        if (!moveFound)
            return;

        isWhiteTurn = !isWhiteTurn;
        update();

        if (matrix.getCheckKingLoc() != null) {
            if (possibleMoves.size() == 0) {
                if (mateHandler != null) {
                    mateHandler.run(matrix.getCheckKingLoc());
                }
            } else if (checkHandler != null) {
                checkHandler.run(matrix.getCheckKingLoc());
            }
            matrix.setCheckKingLoc(null);
        }
    }
}
